package schoolManagement;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import javax.swing.JOptionPane;

/**
 *
 * @author Afarin
 */
//PassWord:tFzwx6YuIABHn3Yu
//UsrName:school
//database:schoolsystem ,table:student
public class Database {

    public static final String DBUSER = "school";
    public static final String DBPASS = "tFzwx6YuIABHn3Yu";
    ResultSet rs = null;

    private Connection conn;

    public Database() throws SQLException {
        try {
            Class.forName("com.mysql.jdbc.Driver");
            conn = DriverManager.getConnection("jdbc:mysql://localhost/schoolsystem", DBUSER, DBPASS);

        } catch (ClassNotFoundException ex) {
            throw new SQLException("Driver class not found", ex);
        }
    }

    // Method for adding data in to database
    public void addStudent(Students s) throws SQLException {
        String sql = "INSERT INTO student(firstname,lastname,age,gender,classlevel,address,city,phone) VALUES (?,?,?,?,?,?,?,?)";
        try (PreparedStatement stmt = conn.prepareStatement(sql)) {
            stmt.setString(1, s.firstName);
            stmt.setString(2, s.lastName);
            stmt.setInt(3, s.age);
            stmt.setString(4, s.gender);
            stmt.setString(5, s.classLecvel);
            stmt.setString(6, s.address);
            stmt.setString(7, s.city);
            stmt.setString(8, s.phone);
            stmt.executeUpdate();
        }
    }

    // Method for deleting data from database
    public void deleteStudent(long id) throws SQLException {
        String sql = "DELETE FROM student WHERE ID=?";
        try (PreparedStatement stmt = conn.prepareStatement(sql)) {
            stmt.setLong(1, id);
            stmt.executeUpdate();
        }
    }

    //method for Searching Student by ID
    public ResultSet findStudent(long id) {

        try {
            String sql = ("SELECT * FROM  student WHERE ID = ?");
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setLong(1, id);
            rs = stmt.executeQuery();
        } catch (SQLException ex) {
            JOptionPane.showMessageDialog(null, ex.getMessage());
        }
        return rs;

    }

    //Method for retrieving all of values from JDBC
    public ArrayList<Students> getAllStudents() throws SQLException {
        ArrayList<Students> studentList = new ArrayList<>();
        String sql = "SELECT * FROM student";
        try (Statement stmt = conn.createStatement(); ResultSet rs = stmt.executeQuery(sql)) {
            Students s = new Students();
            while (rs.next()) {
                s.id = rs.getLong("ID");
                s.firstName = rs.getString("firstname");
                s.lastName = rs.getString("lastname");
                s.age = rs.getInt("age");
                s.gender = rs.getString("gender");
                s.classLecvel = rs.getString("classlevel");
                s.address = rs.getString("address");
                s.city = rs.getString("city");
                s.phone = rs.getString("phone");
                studentList.add(s);

            }
        }
        return studentList;
    }
    
    public void updateStudent(Students s) throws SQLException{
        String sql ="UPDATE student SET firstname=?,lastname=?,age=?,gender=?,classlevel=?,address=?,city=?,phone=? WHERE ID =?";
        try(PreparedStatement stmt=conn.prepareStatement(sql)){
          //  stmt.setLong(1,s.id);
            stmt.setString(1,s.firstName);
            stmt.setString(2, s.lastName);
            stmt.setInt(3, s.age);
            stmt.setString(4, s.gender);
            stmt.setString(5, s.classLecvel);
            stmt.setString(6, s.address);
            stmt.setString(7, s.city);
            stmt.setString(8, s.phone);
            stmt.setLong(9,s.id);
            stmt.executeUpdate();
        }
        
    }
    //Method for teachers
     public ArrayList<Teachers> getAllteacher() throws SQLException {
        ArrayList<Teachers> list = new ArrayList<>();
        String sql = "SELECT * FROM teacher";
        try (Statement stmt = conn.createStatement(); ResultSet result = stmt.executeQuery(sql)) {
            while (result.next()) {
                Teachers t = new Teachers();
                t.id = result.getLong("ID");
                t.firstName = result.getString("firstname");
                t.lastName = result.getString("lastname");
                t.program = result.getString("program");
                list.add(t);
            }
        }
        return list;
    }


}
