package schoolManagement;

import java.awt.HeadlessException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import javax.swing.DefaultListModel;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import net.proteanit.sql.DbUtils;

/**
 *
 * @author Afarin
 */
public class SchoolManagementSys extends javax.swing.JFrame {

    Database db;
    DefaultListModel<Teachers> teacherListModel = new DefaultListModel<>();

    private void reloadTeacher() {
        try {
            ArrayList<Teachers> teacherList = db.getAllteacher();
            teacherListModel.clear();
            for (Teachers t : teacherList) {
                teacherListModel.addElement(t);
            }
        } catch (SQLException ex) {
            ex.printStackTrace();
            JOptionPane.showMessageDialog(this,
                    ex.getMessage(),
                    "Database access error",
                    JOptionPane.ERROR_MESSAGE);
        }
    }
    //////////////////////////////////////////
    public static final String DBUSER = "school";
    public static final String DBPASS = "tFzwx6YuIABHn3Yu";
    ResultSet rs = null;

    private Connection conn;

    public void showTable() throws SQLException {
        try {
            Class.forName("com.mysql.jdbc.Driver");
            conn = DriverManager.getConnection("jdbc:mysql://localhost/schoolsystem", DBUSER, DBPASS);

        } catch (ClassNotFoundException ex) {
            throw new SQLException("Driver class not found", ex);
        }
        String sql = "SELECT * FROM student";
        try (PreparedStatement stmt = conn.prepareStatement(sql)) {
            rs = stmt.executeQuery();
            tbl_ViewStudent.setModel(DbUtils.resultSetToTableModel(rs));

        }
    }
    //////////////////////////////////////////////

    public SchoolManagementSys() {
        try {
            db = new Database();

            initComponents();
            showTable();
            reloadTeacher();

        } catch (SQLException e) {
            JOptionPane.showMessageDialog(this,
                    e.getMessage(),
                    "Fatal error opening database connection",
                    JOptionPane.ERROR_MESSAGE);
            System.exit(1);
        }
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        framePassWord = new javax.swing.JFrame();
        framePassword_panelBody = new javax.swing.JPanel();
        framePassWord_panelTop = new javax.swing.JPanel();
        framePassWord_X = new javax.swing.JLabel();
        framePassWord_lblLogin = new javax.swing.JLabel();
        framePassWord_min = new javax.swing.JLabel();
        framePassWord_lblPass = new javax.swing.JLabel();
        framePassWord_tfUser = new javax.swing.JTextField();
        framePassWord_tfPass = new javax.swing.JPasswordField();
        framePassWord_btnLogin = new javax.swing.JButton();
        framePassWord_lblUser = new javax.swing.JLabel();
        jLabel4 = new javax.swing.JLabel();
        frameStudent = new javax.swing.JFrame();
        frameStudent_panelBody = new javax.swing.JPanel();
        jPanel2 = new javax.swing.JPanel();
        frameStudent_pModificationS = new javax.swing.JTabbedPane();
        frameStudent_PAddStudent = new javax.swing.JPanel();
        jLabel3 = new javax.swing.JLabel();
        jLabel5 = new javax.swing.JLabel();
        frameStudent_tfId = new javax.swing.JTextField();
        frameStudent_tfName = new javax.swing.JTextField();
        jLabel6 = new javax.swing.JLabel();
        frameStudent_tfLastName = new javax.swing.JTextField();
        jLabel8 = new javax.swing.JLabel();
        frameStudent_tfAge = new javax.swing.JTextField();
        jLabel11 = new javax.swing.JLabel();
        frameStudent_cbGender = new javax.swing.JComboBox<>();
        jLabel12 = new javax.swing.JLabel();
        frameStudent_tfLevel = new javax.swing.JTextField();
        jLabel14 = new javax.swing.JLabel();
        frameStudent_tfAddress = new javax.swing.JTextField();
        jLabel15 = new javax.swing.JLabel();
        frameStudent_tfCity = new javax.swing.JTextField();
        jLabel16 = new javax.swing.JLabel();
        frameStudent_tfPhone = new javax.swing.JTextField();
        jPanel5 = new javax.swing.JPanel();
        jLabel17 = new javax.swing.JLabel();
        jLabel18 = new javax.swing.JLabel();
        frameStudent_btnAdd = new javax.swing.JButton();
        jPanel10 = new javax.swing.JPanel();
        frameStudent_tpDelete = new javax.swing.JTabbedPane();
        frameStudent_pDelete = new javax.swing.JPanel();
        jPanel7 = new javax.swing.JPanel();
        jLabel24 = new javax.swing.JLabel();
        jLabel25 = new javax.swing.JLabel();
        jLabel47 = new javax.swing.JLabel();
        jLabel28 = new javax.swing.JLabel();
        frameStudent_tfIdDelete = new javax.swing.JTextField();
        jLabel30 = new javax.swing.JLabel();
        frameStudent_tfNameDel = new javax.swing.JTextField();
        jLabel31 = new javax.swing.JLabel();
        frameStudent_tfLastNameDel = new javax.swing.JTextField();
        jLabel32 = new javax.swing.JLabel();
        frameStudent_tfAgeDel = new javax.swing.JTextField();
        jLabel33 = new javax.swing.JLabel();
        jLabel41 = new javax.swing.JLabel();
        frameStudent_tfLevelDel = new javax.swing.JTextField();
        jLabel43 = new javax.swing.JLabel();
        frameStudent_tfAddressDel = new javax.swing.JTextField();
        jLabel44 = new javax.swing.JLabel();
        frameStudent_tfCityDel = new javax.swing.JTextField();
        jLabel45 = new javax.swing.JLabel();
        frameStudent_tfPhoneDelete = new javax.swing.JTextField();
        frameStudent_btnDelete1 = new javax.swing.JButton();
        frameStudent_tfDeleteStu = new javax.swing.JTextField();
        jLabel54 = new javax.swing.JLabel();
        frameStudent_tfGenderDel = new javax.swing.JTextField();
        frameStudent_pUpdate = new javax.swing.JPanel();
        jPanel12 = new javax.swing.JPanel();
        jLabel26 = new javax.swing.JLabel();
        jLabel27 = new javax.swing.JLabel();
        jLabel46 = new javax.swing.JLabel();
        frameStudent_btnUpdate = new javax.swing.JButton();
        jLabel29 = new javax.swing.JLabel();
        frameStudent_tfIdUpdate = new javax.swing.JTextField();
        jLabel34 = new javax.swing.JLabel();
        frameStudent_tfAgeUpdate = new javax.swing.JTextField();
        jLabel35 = new javax.swing.JLabel();
        jLabel36 = new javax.swing.JLabel();
        frameStudent_tfNameUpdate = new javax.swing.JTextField();
        jLabel37 = new javax.swing.JLabel();
        frameStudent_tfLastNameUpdate = new javax.swing.JTextField();
        jLabel38 = new javax.swing.JLabel();
        frameStudent_tfLevelUpdate = new javax.swing.JTextField();
        jLabel40 = new javax.swing.JLabel();
        frameStudent_tfAddressUpdate = new javax.swing.JTextField();
        jLabel42 = new javax.swing.JLabel();
        frameStudent_tfUpdateEnterId = new javax.swing.JTextField();
        jLabel39 = new javax.swing.JLabel();
        frameStudent_tfCityUpdate = new javax.swing.JTextField();
        jLabel60 = new javax.swing.JLabel();
        frameStudent_tfPhoneUpdate = new javax.swing.JTextField();
        frameStudent_cbGenderUpdate = new javax.swing.JComboBox<>();
        jPanel11 = new javax.swing.JPanel();
        frameStudent_PAddStudent3 = new javax.swing.JPanel();
        jLabel48 = new javax.swing.JLabel();
        jLabel49 = new javax.swing.JLabel();
        frameStudent_tfIdSearch = new javax.swing.JTextField();
        frameStudent_tfNameSearch = new javax.swing.JTextField();
        jLabel50 = new javax.swing.JLabel();
        frameStudent_tfLNSearch = new javax.swing.JTextField();
        jLabel51 = new javax.swing.JLabel();
        frameStudent_tfAgeSearch = new javax.swing.JTextField();
        jLabel52 = new javax.swing.JLabel();
        jLabel53 = new javax.swing.JLabel();
        frameStudent_tfLevelSearch = new javax.swing.JTextField();
        jLabel55 = new javax.swing.JLabel();
        frameStudent_tfAddressSearch = new javax.swing.JTextField();
        jLabel56 = new javax.swing.JLabel();
        frameStudent_tfCitySearch = new javax.swing.JTextField();
        jLabel57 = new javax.swing.JLabel();
        frameStudent_tfPhoneSearch = new javax.swing.JTextField();
        jPanel16 = new javax.swing.JPanel();
        jLabel58 = new javax.swing.JLabel();
        jLabel59 = new javax.swing.JLabel();
        frameStudent_btnSearch = new javax.swing.JButton();
        frameStudent_tfEnterPass = new javax.swing.JTextField();
        frameStudent_tfSearchGender = new javax.swing.JTextField();
        jLabel13 = new javax.swing.JLabel();
        frameStudent_btnSearchReset = new javax.swing.JButton();
        jPanel15 = new javax.swing.JPanel();
        jScrollPane1 = new javax.swing.JScrollPane();
        tbl_ViewStudent = new javax.swing.JTable();
        jPanel4 = new javax.swing.JPanel();
        jLabel1 = new javax.swing.JLabel();
        jTabbedPane4 = new javax.swing.JTabbedPane();
        jLabel19 = new javax.swing.JLabel();
        jPanel6 = new javax.swing.JPanel();
        jLabel20 = new javax.swing.JLabel();
        jLabel21 = new javax.swing.JLabel();
        jLabel22 = new javax.swing.JLabel();
        jLabel23 = new javax.swing.JLabel();
        jTabbedPane10 = new javax.swing.JTabbedPane();
        jPanel8 = new javax.swing.JPanel();
        jPanel9 = new javax.swing.JPanel();
        jTabbedPane11 = new javax.swing.JTabbedPane();
        jTabbedPane8 = new javax.swing.JTabbedPane();
        jPanel13 = new javax.swing.JPanel();
        jPanel14 = new javax.swing.JPanel();
        jPanel3 = new javax.swing.JPanel();
        frameTeachers = new javax.swing.JFrame();
        frameTeachers_panelBody = new javax.swing.JPanel();
        jPanel17 = new javax.swing.JPanel();
        jLabel61 = new javax.swing.JLabel();
        jLabel62 = new javax.swing.JLabel();
        jLabel64 = new javax.swing.JLabel();
        jLabel65 = new javax.swing.JLabel();
        jScrollPane2 = new javax.swing.JScrollPane();
        frameTeacher_lstTeacher = new javax.swing.JList<>();
        jLabel63 = new javax.swing.JLabel();
        jLabel71 = new javax.swing.JLabel();
        panelBack = new javax.swing.JPanel();
        panelSideLeft = new javax.swing.JPanel();
        btn_schoolAdmission = new javax.swing.JButton();
        btn_schoolFee1 = new javax.swing.JButton();
        jPanel1 = new javax.swing.JPanel();
        jLabel10 = new javax.swing.JLabel();
        lblExit = new javax.swing.JLabel();
        lblMin = new javax.swing.JLabel();
        btnTeachers = new javax.swing.JButton();
        btnStudents = new javax.swing.JButton();
        btnLogin = new javax.swing.JButton();
        jLabel7 = new javax.swing.JLabel();
        jLabel9 = new javax.swing.JLabel();
        jLabel2 = new javax.swing.JLabel();

        framePassWord.setBackground(new java.awt.Color(255, 247, 239));
        framePassWord.setLocationByPlatform(true);
        framePassWord.setUndecorated(true);

        framePassword_panelBody.setBackground(new java.awt.Color(200, 247, 197));

        framePassWord_panelTop.setBackground(new java.awt.Color(38, 166, 91));

        framePassWord_X.setFont(new java.awt.Font("Tahoma", 1, 18)); // NOI18N
        framePassWord_X.setText("X");
        framePassWord_X.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mousePressed(java.awt.event.MouseEvent evt) {
                framePassWord_XMousePressed(evt);
            }
        });

        framePassWord_lblLogin.setFont(new java.awt.Font("Tahoma", 1, 36)); // NOI18N
        framePassWord_lblLogin.setText("Login");

        framePassWord_min.setFont(new java.awt.Font("Tahoma", 1, 18)); // NOI18N
        framePassWord_min.setText("-");
        framePassWord_min.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mousePressed(java.awt.event.MouseEvent evt) {
                framePassWord_minMousePressed(evt);
            }
        });

        javax.swing.GroupLayout framePassWord_panelTopLayout = new javax.swing.GroupLayout(framePassWord_panelTop);
        framePassWord_panelTop.setLayout(framePassWord_panelTopLayout);
        framePassWord_panelTopLayout.setHorizontalGroup(
            framePassWord_panelTopLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, framePassWord_panelTopLayout.createSequentialGroup()
                .addGap(0, 0, Short.MAX_VALUE)
                .addComponent(framePassWord_min, javax.swing.GroupLayout.PREFERRED_SIZE, 25, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(framePassWord_X, javax.swing.GroupLayout.PREFERRED_SIZE, 25, javax.swing.GroupLayout.PREFERRED_SIZE))
            .addGroup(framePassWord_panelTopLayout.createSequentialGroup()
                .addGap(38, 38, 38)
                .addComponent(framePassWord_lblLogin)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        framePassWord_panelTopLayout.setVerticalGroup(
            framePassWord_panelTopLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(framePassWord_panelTopLayout.createSequentialGroup()
                .addGroup(framePassWord_panelTopLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(framePassWord_X, javax.swing.GroupLayout.PREFERRED_SIZE, 45, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(framePassWord_min, javax.swing.GroupLayout.PREFERRED_SIZE, 45, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(framePassWord_lblLogin, javax.swing.GroupLayout.PREFERRED_SIZE, 45, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(0, 32, Short.MAX_VALUE))
        );

        framePassWord_lblPass.setFont(new java.awt.Font("Tahoma", 1, 24)); // NOI18N
        framePassWord_lblPass.setIcon(new javax.swing.ImageIcon(getClass().getResource("/schoolManagement/images/icons8_Lock_52px_2.png"))); // NOI18N
        framePassWord_lblPass.setToolTipText("");

        framePassWord_btnLogin.setBackground(new java.awt.Color(255, 221, 153));
        framePassWord_btnLogin.setFont(new java.awt.Font("Tahoma", 1, 18)); // NOI18N
        framePassWord_btnLogin.setText("Login");
        framePassWord_btnLogin.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                framePassWord_btnLoginActionPerformed(evt);
            }
        });

        framePassWord_lblUser.setIcon(new javax.swing.ImageIcon(getClass().getResource("/schoolManagement/images/icons8_User_Male_48px.png"))); // NOI18N

        javax.swing.GroupLayout framePassword_panelBodyLayout = new javax.swing.GroupLayout(framePassword_panelBody);
        framePassword_panelBody.setLayout(framePassword_panelBodyLayout);
        framePassword_panelBodyLayout.setHorizontalGroup(
            framePassword_panelBodyLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(framePassWord_panelTop, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addGroup(framePassword_panelBodyLayout.createSequentialGroup()
                .addGap(41, 41, 41)
                .addGroup(framePassword_panelBodyLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(framePassWord_lblPass, javax.swing.GroupLayout.PREFERRED_SIZE, 73, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, framePassword_panelBodyLayout.createSequentialGroup()
                        .addComponent(framePassWord_lblUser)
                        .addGap(16, 16, 16)))
                .addGroup(framePassword_panelBodyLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(framePassWord_tfUser, javax.swing.GroupLayout.PREFERRED_SIZE, 278, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(framePassWord_tfPass, javax.swing.GroupLayout.PREFERRED_SIZE, 314, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap(35, Short.MAX_VALUE))
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, framePassword_panelBodyLayout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(framePassWord_btnLogin, javax.swing.GroupLayout.PREFERRED_SIZE, 161, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(157, 157, 157))
        );

        framePassword_panelBodyLayout.linkSize(javax.swing.SwingConstants.HORIZONTAL, new java.awt.Component[] {framePassWord_tfPass, framePassWord_tfUser});

        framePassword_panelBodyLayout.setVerticalGroup(
            framePassword_panelBodyLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(framePassword_panelBodyLayout.createSequentialGroup()
                .addComponent(framePassWord_panelTop, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(89, 89, 89)
                .addGroup(framePassword_panelBodyLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(framePassWord_tfUser, javax.swing.GroupLayout.PREFERRED_SIZE, 46, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(framePassWord_lblUser))
                .addGroup(framePassword_panelBodyLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(framePassword_panelBodyLayout.createSequentialGroup()
                        .addGap(20, 20, 20)
                        .addComponent(framePassWord_tfPass, javax.swing.GroupLayout.PREFERRED_SIZE, 50, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(framePassword_panelBodyLayout.createSequentialGroup()
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(framePassWord_lblPass)))
                .addGap(27, 27, 27)
                .addComponent(framePassWord_btnLogin, javax.swing.GroupLayout.PREFERRED_SIZE, 57, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(0, 72, Short.MAX_VALUE))
        );

        javax.swing.GroupLayout framePassWordLayout = new javax.swing.GroupLayout(framePassWord.getContentPane());
        framePassWord.getContentPane().setLayout(framePassWordLayout);
        framePassWordLayout.setHorizontalGroup(
            framePassWordLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(framePassword_panelBody, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        framePassWordLayout.setVerticalGroup(
            framePassWordLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(framePassword_panelBody, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );

        jLabel4.setFont(new java.awt.Font("Tahoma", 1, 24)); // NOI18N
        jLabel4.setText("Username:");

        frameStudent_panelBody.setBackground(new java.awt.Color(200, 247, 197));

        jPanel2.setBackground(new java.awt.Color(200, 247, 197));

        frameStudent_pModificationS.setBackground(new java.awt.Color(200, 247, 197));

        frameStudent_PAddStudent.setBackground(new java.awt.Color(200, 247, 197));

        jLabel3.setFont(new java.awt.Font("Tahoma", 1, 16)); // NOI18N
        jLabel3.setText("Student ID");

        jLabel5.setFont(new java.awt.Font("Tahoma", 1, 16)); // NOI18N
        jLabel5.setText("First Name");

        jLabel6.setFont(new java.awt.Font("Tahoma", 1, 16)); // NOI18N
        jLabel6.setText("Last Name");

        jLabel8.setFont(new java.awt.Font("Tahoma", 1, 16)); // NOI18N
        jLabel8.setText("Age");

        jLabel11.setFont(new java.awt.Font("Tahoma", 1, 16)); // NOI18N
        jLabel11.setText("Gender");

        frameStudent_cbGender.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "Select", "Female", "Male", " " }));

        jLabel12.setFont(new java.awt.Font("Tahoma", 1, 16)); // NOI18N
        jLabel12.setText("Class Level");

        jLabel14.setFont(new java.awt.Font("Tahoma", 1, 16)); // NOI18N
        jLabel14.setText("Address");

        jLabel15.setFont(new java.awt.Font("Tahoma", 1, 16)); // NOI18N
        jLabel15.setText("City");

        jLabel16.setFont(new java.awt.Font("Tahoma", 1, 16)); // NOI18N
        jLabel16.setText("Phone No.");

        jPanel5.setBackground(new java.awt.Color(38, 166, 91));

        jLabel17.setFont(new java.awt.Font("Tahoma", 1, 36)); // NOI18N
        jLabel17.setText("Add Student");

        jLabel18.setIcon(new javax.swing.ImageIcon(getClass().getResource("/schoolManagement/images/icons8_Students_100px_1.png"))); // NOI18N
        jLabel18.setText("\n");

        javax.swing.GroupLayout jPanel5Layout = new javax.swing.GroupLayout(jPanel5);
        jPanel5.setLayout(jPanel5Layout);
        jPanel5Layout.setHorizontalGroup(
            jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel5Layout.createSequentialGroup()
                .addGap(39, 39, 39)
                .addComponent(jLabel18, javax.swing.GroupLayout.PREFERRED_SIZE, 106, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jLabel17)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        jPanel5Layout.setVerticalGroup(
            jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel5Layout.createSequentialGroup()
                .addContainerGap(34, Short.MAX_VALUE)
                .addGroup(jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel5Layout.createSequentialGroup()
                        .addComponent(jLabel18, javax.swing.GroupLayout.PREFERRED_SIZE, 116, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addContainerGap())
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel5Layout.createSequentialGroup()
                        .addComponent(jLabel17)
                        .addGap(44, 44, 44))))
        );

        frameStudent_btnAdd.setBackground(new java.awt.Color(38, 166, 91));
        frameStudent_btnAdd.setFont(new java.awt.Font("Tahoma", 1, 16)); // NOI18N
        frameStudent_btnAdd.setText("Add");
        frameStudent_btnAdd.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                frameStudent_btnAddActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout frameStudent_PAddStudentLayout = new javax.swing.GroupLayout(frameStudent_PAddStudent);
        frameStudent_PAddStudent.setLayout(frameStudent_PAddStudentLayout);
        frameStudent_PAddStudentLayout.setHorizontalGroup(
            frameStudent_PAddStudentLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel5, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addGroup(frameStudent_PAddStudentLayout.createSequentialGroup()
                .addGap(72, 72, 72)
                .addGroup(frameStudent_PAddStudentLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(jLabel12)
                    .addComponent(jLabel3)
                    .addGroup(frameStudent_PAddStudentLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addComponent(jLabel8)
                        .addComponent(jLabel5))
                    .addComponent(jLabel14, javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel15, javax.swing.GroupLayout.Alignment.LEADING))
                .addGap(18, 18, 18)
                .addGroup(frameStudent_PAddStudentLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(frameStudent_PAddStudentLayout.createSequentialGroup()
                        .addGroup(frameStudent_PAddStudentLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(frameStudent_PAddStudentLayout.createSequentialGroup()
                                .addComponent(frameStudent_tfCity, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(63, 63, 63)
                                .addComponent(jLabel16)
                                .addGap(32, 32, 32)
                                .addComponent(frameStudent_tfPhone, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addGroup(frameStudent_PAddStudentLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                                .addComponent(frameStudent_tfAddress, javax.swing.GroupLayout.Alignment.LEADING)
                                .addGroup(javax.swing.GroupLayout.Alignment.LEADING, frameStudent_PAddStudentLayout.createSequentialGroup()
                                    .addGroup(frameStudent_PAddStudentLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                        .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, frameStudent_PAddStudentLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                            .addComponent(frameStudent_tfName, javax.swing.GroupLayout.Alignment.TRAILING)
                                            .addComponent(frameStudent_tfAge, javax.swing.GroupLayout.PREFERRED_SIZE, 199, javax.swing.GroupLayout.PREFERRED_SIZE))
                                        .addComponent(frameStudent_tfLevel, javax.swing.GroupLayout.PREFERRED_SIZE, 199, javax.swing.GroupLayout.PREFERRED_SIZE))
                                    .addGap(63, 63, 63)
                                    .addGroup(frameStudent_PAddStudentLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                        .addComponent(jLabel11)
                                        .addComponent(jLabel6))
                                    .addGap(28, 28, 28)
                                    .addGroup(frameStudent_PAddStudentLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                                        .addComponent(frameStudent_tfLastName, javax.swing.GroupLayout.DEFAULT_SIZE, 199, Short.MAX_VALUE)
                                        .addComponent(frameStudent_cbGender, javax.swing.GroupLayout.PREFERRED_SIZE, 199, javax.swing.GroupLayout.PREFERRED_SIZE)))))
                        .addContainerGap(178, Short.MAX_VALUE))
                    .addGroup(frameStudent_PAddStudentLayout.createSequentialGroup()
                        .addComponent(frameStudent_tfId, javax.swing.GroupLayout.DEFAULT_SIZE, 163, Short.MAX_VALUE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(frameStudent_btnAdd, javax.swing.GroupLayout.PREFERRED_SIZE, 87, javax.swing.GroupLayout.PREFERRED_SIZE))))
        );

        frameStudent_PAddStudentLayout.linkSize(javax.swing.SwingConstants.HORIZONTAL, new java.awt.Component[] {frameStudent_cbGender, frameStudent_tfAge, frameStudent_tfCity, frameStudent_tfId, frameStudent_tfLastName, frameStudent_tfLevel, frameStudent_tfName, frameStudent_tfPhone});

        frameStudent_PAddStudentLayout.setVerticalGroup(
            frameStudent_PAddStudentLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(frameStudent_PAddStudentLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jPanel5, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(51, 51, 51)
                .addGroup(frameStudent_PAddStudentLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, frameStudent_PAddStudentLayout.createSequentialGroup()
                        .addGroup(frameStudent_PAddStudentLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(frameStudent_tfId, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jLabel3)
                            .addComponent(frameStudent_btnAdd))
                        .addGap(70, 70, 70)
                        .addGroup(frameStudent_PAddStudentLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(frameStudent_tfName, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jLabel5)
                            .addComponent(jLabel6)
                            .addComponent(frameStudent_tfLastName, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addGroup(frameStudent_PAddStudentLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabel8)
                            .addComponent(frameStudent_tfAge, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jLabel11)
                            .addComponent(frameStudent_cbGender, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(18, 18, 18)
                        .addGroup(frameStudent_PAddStudentLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabel12)
                            .addComponent(frameStudent_tfLevel, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(18, 18, 18)
                        .addGroup(frameStudent_PAddStudentLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabel14)
                            .addComponent(frameStudent_tfAddress, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(54, 54, 54))
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, frameStudent_PAddStudentLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(frameStudent_tfCity, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(jLabel16)
                        .addComponent(frameStudent_tfPhone, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(jLabel15)))
                .addContainerGap(157, Short.MAX_VALUE))
        );

        frameStudent_pModificationS.addTab("Student", frameStudent_PAddStudent);

        jPanel10.setBackground(new java.awt.Color(200, 247, 197));

        frameStudent_tpDelete.setBackground(new java.awt.Color(200, 247, 197));

        frameStudent_pDelete.setBackground(new java.awt.Color(200, 247, 197));

        jPanel7.setBackground(new java.awt.Color(38, 166, 91));

        jLabel24.setFont(new java.awt.Font("Tahoma", 1, 36)); // NOI18N
        jLabel24.setText("Delete Student");

        jLabel25.setIcon(new javax.swing.ImageIcon(getClass().getResource("/schoolManagement/images/icons8_Students_100px_1.png"))); // NOI18N
        jLabel25.setText("\n");

        jLabel47.setIcon(new javax.swing.ImageIcon(getClass().getResource("/schoolManagement/images/icons8_Delete_52px_1.png"))); // NOI18N

        javax.swing.GroupLayout jPanel7Layout = new javax.swing.GroupLayout(jPanel7);
        jPanel7.setLayout(jPanel7Layout);
        jPanel7Layout.setHorizontalGroup(
            jPanel7Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel7Layout.createSequentialGroup()
                .addGap(27, 27, 27)
                .addComponent(jLabel25, javax.swing.GroupLayout.PREFERRED_SIZE, 106, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addComponent(jLabel47)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jLabel24)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        jPanel7Layout.setVerticalGroup(
            jPanel7Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel7Layout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGroup(jPanel7Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel7Layout.createSequentialGroup()
                        .addComponent(jLabel25, javax.swing.GroupLayout.PREFERRED_SIZE, 116, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addContainerGap())
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel7Layout.createSequentialGroup()
                        .addGroup(jPanel7Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel47)
                            .addComponent(jLabel24))
                        .addGap(43, 43, 43))))
        );

        jLabel28.setFont(new java.awt.Font("Tahoma", 1, 16)); // NOI18N
        jLabel28.setText("Student ID");

        jLabel30.setFont(new java.awt.Font("Tahoma", 1, 16)); // NOI18N
        jLabel30.setText("First Name");

        jLabel31.setFont(new java.awt.Font("Tahoma", 1, 16)); // NOI18N
        jLabel31.setText("Last Name");

        jLabel32.setFont(new java.awt.Font("Tahoma", 1, 16)); // NOI18N
        jLabel32.setText("Age");

        frameStudent_tfAgeDel.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                frameStudent_tfAgeDelActionPerformed(evt);
            }
        });

        jLabel33.setFont(new java.awt.Font("Tahoma", 1, 16)); // NOI18N
        jLabel33.setText("Gender");

        jLabel41.setFont(new java.awt.Font("Tahoma", 1, 16)); // NOI18N
        jLabel41.setText("Class Level");

        jLabel43.setFont(new java.awt.Font("Tahoma", 1, 16)); // NOI18N
        jLabel43.setText("Address");

        jLabel44.setFont(new java.awt.Font("Tahoma", 1, 16)); // NOI18N
        jLabel44.setText("City");

        jLabel45.setFont(new java.awt.Font("Tahoma", 1, 16)); // NOI18N
        jLabel45.setText("Phone No.");

        frameStudent_btnDelete1.setBackground(new java.awt.Color(38, 166, 91));
        frameStudent_btnDelete1.setFont(new java.awt.Font("Tahoma", 1, 16)); // NOI18N
        frameStudent_btnDelete1.setText("Delete");
        frameStudent_btnDelete1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                frameStudent_btnDelete1ActionPerformed(evt);
            }
        });

        jLabel54.setFont(new java.awt.Font("Tahoma", 1, 16)); // NOI18N
        jLabel54.setText("Enter Student's ID");

        javax.swing.GroupLayout frameStudent_pDeleteLayout = new javax.swing.GroupLayout(frameStudent_pDelete);
        frameStudent_pDelete.setLayout(frameStudent_pDeleteLayout);
        frameStudent_pDeleteLayout.setHorizontalGroup(
            frameStudent_pDeleteLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel7, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addGroup(frameStudent_pDeleteLayout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGroup(frameStudent_pDeleteLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel43)
                    .addComponent(jLabel44)
                    .addComponent(jLabel41)
                    .addComponent(jLabel32)
                    .addComponent(jLabel54)
                    .addComponent(jLabel28))
                .addGap(37, 37, 37)
                .addGroup(frameStudent_pDeleteLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(frameStudent_pDeleteLayout.createSequentialGroup()
                        .addComponent(frameStudent_tfAddressDel)
                        .addGap(0, 0, Short.MAX_VALUE))
                    .addGroup(frameStudent_pDeleteLayout.createSequentialGroup()
                        .addGroup(frameStudent_pDeleteLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(frameStudent_tfCityDel)
                            .addGroup(frameStudent_pDeleteLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                                .addComponent(frameStudent_tfIdDelete, javax.swing.GroupLayout.PREFERRED_SIZE, 191, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGroup(frameStudent_pDeleteLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                    .addComponent(frameStudent_tfLevelDel, javax.swing.GroupLayout.DEFAULT_SIZE, 191, Short.MAX_VALUE)
                                    .addComponent(frameStudent_tfAgeDel))
                                .addComponent(frameStudent_tfDeleteStu, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                        .addGroup(frameStudent_pDeleteLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(frameStudent_pDeleteLayout.createSequentialGroup()
                                .addGap(68, 68, 68)
                                .addGroup(frameStudent_pDeleteLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addGroup(frameStudent_pDeleteLayout.createSequentialGroup()
                                        .addComponent(jLabel31)
                                        .addGap(31, 31, 31)
                                        .addComponent(frameStudent_tfLastNameDel, javax.swing.GroupLayout.PREFERRED_SIZE, 186, javax.swing.GroupLayout.PREFERRED_SIZE))
                                    .addGroup(frameStudent_pDeleteLayout.createSequentialGroup()
                                        .addComponent(jLabel33)
                                        .addGap(58, 58, 58)
                                        .addComponent(frameStudent_tfGenderDel, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                                    .addGroup(frameStudent_pDeleteLayout.createSequentialGroup()
                                        .addComponent(jLabel45)
                                        .addGap(34, 34, 34)
                                        .addComponent(frameStudent_tfPhoneDelete, javax.swing.GroupLayout.PREFERRED_SIZE, 144, javax.swing.GroupLayout.PREFERRED_SIZE))
                                    .addGroup(frameStudent_pDeleteLayout.createSequentialGroup()
                                        .addComponent(jLabel30)
                                        .addGap(29, 29, 29)
                                        .addComponent(frameStudent_tfNameDel, javax.swing.GroupLayout.PREFERRED_SIZE, 191, javax.swing.GroupLayout.PREFERRED_SIZE)))
                                .addGap(37, 37, 37))
                            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, frameStudent_pDeleteLayout.createSequentialGroup()
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(frameStudent_btnDelete1, javax.swing.GroupLayout.PREFERRED_SIZE, 95, javax.swing.GroupLayout.PREFERRED_SIZE))))))
        );

        frameStudent_pDeleteLayout.linkSize(javax.swing.SwingConstants.HORIZONTAL, new java.awt.Component[] {frameStudent_tfDeleteStu, frameStudent_tfIdDelete});

        frameStudent_pDeleteLayout.linkSize(javax.swing.SwingConstants.HORIZONTAL, new java.awt.Component[] {frameStudent_tfGenderDel, frameStudent_tfLastNameDel, frameStudent_tfPhoneDelete});

        frameStudent_pDeleteLayout.setVerticalGroup(
            frameStudent_pDeleteLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(frameStudent_pDeleteLayout.createSequentialGroup()
                .addComponent(jPanel7, javax.swing.GroupLayout.PREFERRED_SIZE, 141, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(70, 70, 70)
                .addGroup(frameStudent_pDeleteLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(frameStudent_btnDelete1)
                    .addComponent(frameStudent_tfDeleteStu, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel54))
                .addGap(59, 59, 59)
                .addGroup(frameStudent_pDeleteLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(frameStudent_pDeleteLayout.createSequentialGroup()
                        .addGroup(frameStudent_pDeleteLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                            .addComponent(jLabel30)
                            .addComponent(frameStudent_tfNameDel, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(18, 18, 18)
                        .addGroup(frameStudent_pDeleteLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabel32)
                            .addComponent(frameStudent_tfAgeDel, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jLabel31)
                            .addComponent(frameStudent_tfLastNameDel, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGroup(frameStudent_pDeleteLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(frameStudent_pDeleteLayout.createSequentialGroup()
                                .addGap(28, 28, 28)
                                .addGroup(frameStudent_pDeleteLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                                    .addComponent(jLabel41)
                                    .addComponent(frameStudent_tfLevelDel, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                                .addGap(61, 61, 61)
                                .addGroup(frameStudent_pDeleteLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                    .addComponent(jLabel43)
                                    .addComponent(frameStudent_tfAddressDel, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                                .addGap(40, 40, 40)
                                .addGroup(frameStudent_pDeleteLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                    .addComponent(frameStudent_tfCityDel, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addComponent(jLabel44)
                                    .addComponent(jLabel45)
                                    .addComponent(frameStudent_tfPhoneDelete, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                            .addGroup(frameStudent_pDeleteLayout.createSequentialGroup()
                                .addGap(18, 18, 18)
                                .addGroup(frameStudent_pDeleteLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                    .addComponent(frameStudent_tfGenderDel, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addComponent(jLabel33)))))
                    .addGroup(frameStudent_pDeleteLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(jLabel28)
                        .addComponent(frameStudent_tfIdDelete, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addGap(0, 0, Short.MAX_VALUE))
        );

        frameStudent_tpDelete.addTab("Delete", frameStudent_pDelete);

        frameStudent_pUpdate.setBackground(new java.awt.Color(200, 247, 197));

        jPanel12.setBackground(new java.awt.Color(38, 166, 91));

        jLabel26.setFont(new java.awt.Font("Tahoma", 1, 36)); // NOI18N
        jLabel26.setText("Update Student");

        jLabel27.setIcon(new javax.swing.ImageIcon(getClass().getResource("/schoolManagement/images/icons8_Students_100px_1.png"))); // NOI18N
        jLabel27.setText("\n");

        jLabel46.setIcon(new javax.swing.ImageIcon(getClass().getResource("/schoolManagement/images/icons8_Refresh_50px_2.png"))); // NOI18N

        javax.swing.GroupLayout jPanel12Layout = new javax.swing.GroupLayout(jPanel12);
        jPanel12.setLayout(jPanel12Layout);
        jPanel12Layout.setHorizontalGroup(
            jPanel12Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel12Layout.createSequentialGroup()
                .addGap(27, 27, 27)
                .addComponent(jLabel27, javax.swing.GroupLayout.PREFERRED_SIZE, 106, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jLabel46)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jLabel26)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        jPanel12Layout.setVerticalGroup(
            jPanel12Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel12Layout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(jLabel27, javax.swing.GroupLayout.PREFERRED_SIZE, 116, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
            .addGroup(jPanel12Layout.createSequentialGroup()
                .addGap(46, 46, 46)
                .addGroup(jPanel12Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(jLabel26)
                    .addComponent(jLabel46))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        frameStudent_btnUpdate.setBackground(new java.awt.Color(38, 166, 91));
        frameStudent_btnUpdate.setFont(new java.awt.Font("Tahoma", 1, 16)); // NOI18N
        frameStudent_btnUpdate.setText("Update");
        frameStudent_btnUpdate.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                frameStudent_btnUpdateActionPerformed(evt);
            }
        });

        jLabel29.setFont(new java.awt.Font("Tahoma", 1, 16)); // NOI18N
        jLabel29.setText("Student ID");

        jLabel34.setFont(new java.awt.Font("Tahoma", 1, 16)); // NOI18N
        jLabel34.setText("Age");

        jLabel35.setFont(new java.awt.Font("Tahoma", 1, 16)); // NOI18N
        jLabel35.setText("Gender");

        jLabel36.setFont(new java.awt.Font("Tahoma", 1, 16)); // NOI18N
        jLabel36.setText("First Name");

        jLabel37.setFont(new java.awt.Font("Tahoma", 1, 16)); // NOI18N
        jLabel37.setText("Last Name");

        jLabel38.setFont(new java.awt.Font("Tahoma", 1, 16)); // NOI18N
        jLabel38.setText("Class Level");

        jLabel40.setFont(new java.awt.Font("Tahoma", 1, 16)); // NOI18N
        jLabel40.setText("Address");

        jLabel42.setFont(new java.awt.Font("Tahoma", 1, 16)); // NOI18N
        jLabel42.setText("Enter Student's ID");

        jLabel39.setFont(new java.awt.Font("Tahoma", 1, 16)); // NOI18N
        jLabel39.setText("City");

        jLabel60.setFont(new java.awt.Font("Tahoma", 1, 16)); // NOI18N
        jLabel60.setText("Phone No.");

        frameStudent_cbGenderUpdate.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "Select", "Female", "Male", " " }));

        javax.swing.GroupLayout frameStudent_pUpdateLayout = new javax.swing.GroupLayout(frameStudent_pUpdate);
        frameStudent_pUpdate.setLayout(frameStudent_pUpdateLayout);
        frameStudent_pUpdateLayout.setHorizontalGroup(
            frameStudent_pUpdateLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel12, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addGroup(frameStudent_pUpdateLayout.createSequentialGroup()
                .addGap(37, 37, 37)
                .addGroup(frameStudent_pUpdateLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addGroup(frameStudent_pUpdateLayout.createSequentialGroup()
                        .addGroup(frameStudent_pUpdateLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addComponent(jLabel34)
                            .addGroup(frameStudent_pUpdateLayout.createSequentialGroup()
                                .addComponent(jLabel29)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                .addComponent(frameStudent_tfIdUpdate, javax.swing.GroupLayout.PREFERRED_SIZE, 181, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addGroup(frameStudent_pUpdateLayout.createSequentialGroup()
                                .addComponent(jLabel42)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                .addComponent(frameStudent_tfUpdateEnterId, javax.swing.GroupLayout.PREFERRED_SIZE, 129, javax.swing.GroupLayout.PREFERRED_SIZE)))
                        .addGap(365, 365, 365))
                    .addGroup(frameStudent_pUpdateLayout.createSequentialGroup()
                        .addGroup(frameStudent_pUpdateLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel40)
                            .addComponent(jLabel38)
                            .addComponent(jLabel39))
                        .addGap(18, 18, 18)
                        .addGroup(frameStudent_pUpdateLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(frameStudent_tfAddressUpdate)
                            .addGroup(frameStudent_pUpdateLayout.createSequentialGroup()
                                .addGroup(frameStudent_pUpdateLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(frameStudent_tfLevelUpdate, javax.swing.GroupLayout.PREFERRED_SIZE, 163, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addComponent(frameStudent_tfAgeUpdate, javax.swing.GroupLayout.DEFAULT_SIZE, 163, Short.MAX_VALUE)
                                    .addComponent(frameStudent_tfCityUpdate, javax.swing.GroupLayout.PREFERRED_SIZE, 72, javax.swing.GroupLayout.PREFERRED_SIZE))
                                .addGap(48, 48, 48)
                                .addGroup(frameStudent_pUpdateLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(jLabel36)
                                    .addComponent(jLabel37)
                                    .addComponent(jLabel35)
                                    .addComponent(jLabel60))
                                .addGap(36, 36, 36)
                                .addGroup(frameStudent_pUpdateLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(frameStudent_tfPhoneUpdate)
                                    .addComponent(frameStudent_tfLastNameUpdate, javax.swing.GroupLayout.DEFAULT_SIZE, 181, Short.MAX_VALUE)
                                    .addComponent(frameStudent_tfNameUpdate, javax.swing.GroupLayout.DEFAULT_SIZE, 163, Short.MAX_VALUE)
                                    .addComponent(frameStudent_cbGenderUpdate, javax.swing.GroupLayout.PREFERRED_SIZE, 199, javax.swing.GroupLayout.PREFERRED_SIZE))))))
                .addGap(87, 87, 87)
                .addComponent(frameStudent_btnUpdate, javax.swing.GroupLayout.PREFERRED_SIZE, 127, javax.swing.GroupLayout.PREFERRED_SIZE))
        );

        frameStudent_pUpdateLayout.linkSize(javax.swing.SwingConstants.HORIZONTAL, new java.awt.Component[] {frameStudent_cbGenderUpdate, frameStudent_tfAgeUpdate, frameStudent_tfCityUpdate, frameStudent_tfLastNameUpdate, frameStudent_tfLevelUpdate, frameStudent_tfNameUpdate, frameStudent_tfPhoneUpdate});

        frameStudent_pUpdateLayout.setVerticalGroup(
            frameStudent_pUpdateLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(frameStudent_pUpdateLayout.createSequentialGroup()
                .addComponent(jPanel12, javax.swing.GroupLayout.PREFERRED_SIZE, 141, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGroup(frameStudent_pUpdateLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(frameStudent_pUpdateLayout.createSequentialGroup()
                        .addGap(48, 48, 48)
                        .addGroup(frameStudent_pUpdateLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel42)
                            .addComponent(frameStudent_tfUpdateEnterId, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                    .addGroup(frameStudent_pUpdateLayout.createSequentialGroup()
                        .addGap(71, 71, 71)
                        .addComponent(frameStudent_btnUpdate)))
                .addGap(45, 45, 45)
                .addGroup(frameStudent_pUpdateLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel36)
                    .addComponent(frameStudent_tfIdUpdate, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel29)
                    .addComponent(frameStudent_tfNameUpdate, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGroup(frameStudent_pUpdateLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(frameStudent_pUpdateLayout.createSequentialGroup()
                        .addGap(40, 40, 40)
                        .addGroup(frameStudent_pUpdateLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabel34)
                            .addComponent(frameStudent_tfAgeUpdate, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jLabel37)))
                    .addGroup(frameStudent_pUpdateLayout.createSequentialGroup()
                        .addGap(31, 31, 31)
                        .addComponent(frameStudent_tfLastNameUpdate, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addGap(28, 28, 28)
                .addGroup(frameStudent_pUpdateLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel38)
                    .addComponent(frameStudent_tfLevelUpdate, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel35)
                    .addComponent(frameStudent_cbGenderUpdate, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(33, 33, 33)
                .addGroup(frameStudent_pUpdateLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel39)
                    .addComponent(frameStudent_tfCityUpdate, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel60)
                    .addComponent(frameStudent_tfPhoneUpdate, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(35, 35, 35)
                .addGroup(frameStudent_pUpdateLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(frameStudent_tfAddressUpdate, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel40))
                .addContainerGap(113, Short.MAX_VALUE))
        );

        frameStudent_tpDelete.addTab("Update", frameStudent_pUpdate);

        javax.swing.GroupLayout jPanel10Layout = new javax.swing.GroupLayout(jPanel10);
        jPanel10.setLayout(jPanel10Layout);
        jPanel10Layout.setHorizontalGroup(
            jPanel10Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(frameStudent_tpDelete)
        );
        jPanel10Layout.setVerticalGroup(
            jPanel10Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(frameStudent_tpDelete)
        );

        frameStudent_pModificationS.addTab("Modification", jPanel10);

        jPanel11.setBackground(new java.awt.Color(200, 247, 197));

        frameStudent_PAddStudent3.setBackground(new java.awt.Color(200, 247, 197));

        jLabel48.setFont(new java.awt.Font("Tahoma", 1, 16)); // NOI18N
        jLabel48.setText("Student ID");

        jLabel49.setFont(new java.awt.Font("Tahoma", 1, 16)); // NOI18N
        jLabel49.setText("First Name");

        jLabel50.setFont(new java.awt.Font("Tahoma", 1, 16)); // NOI18N
        jLabel50.setText("Last Name");

        jLabel51.setFont(new java.awt.Font("Tahoma", 1, 16)); // NOI18N
        jLabel51.setText("Age");

        jLabel52.setFont(new java.awt.Font("Tahoma", 1, 16)); // NOI18N
        jLabel52.setText("Gender");

        jLabel53.setFont(new java.awt.Font("Tahoma", 1, 16)); // NOI18N
        jLabel53.setText("Class Level");

        jLabel55.setFont(new java.awt.Font("Tahoma", 1, 16)); // NOI18N
        jLabel55.setText("Address");

        jLabel56.setFont(new java.awt.Font("Tahoma", 1, 16)); // NOI18N
        jLabel56.setText("City");

        jLabel57.setFont(new java.awt.Font("Tahoma", 1, 16)); // NOI18N
        jLabel57.setText("Phone No.");

        jPanel16.setBackground(new java.awt.Color(38, 166, 91));

        jLabel58.setFont(new java.awt.Font("Tahoma", 1, 36)); // NOI18N
        jLabel58.setIcon(new javax.swing.ImageIcon(getClass().getResource("/schoolManagement/images/icons8_Search_50px.png"))); // NOI18N
        jLabel58.setText("Search Student");

        jLabel59.setIcon(new javax.swing.ImageIcon(getClass().getResource("/schoolManagement/images/icons8_Students_100px_1.png"))); // NOI18N
        jLabel59.setText("\n");

        javax.swing.GroupLayout jPanel16Layout = new javax.swing.GroupLayout(jPanel16);
        jPanel16.setLayout(jPanel16Layout);
        jPanel16Layout.setHorizontalGroup(
            jPanel16Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel16Layout.createSequentialGroup()
                .addGap(39, 39, 39)
                .addComponent(jLabel59, javax.swing.GroupLayout.PREFERRED_SIZE, 106, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jLabel58)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        jPanel16Layout.setVerticalGroup(
            jPanel16Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel16Layout.createSequentialGroup()
                .addContainerGap(34, Short.MAX_VALUE)
                .addGroup(jPanel16Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel16Layout.createSequentialGroup()
                        .addComponent(jLabel59, javax.swing.GroupLayout.PREFERRED_SIZE, 116, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addContainerGap())
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel16Layout.createSequentialGroup()
                        .addComponent(jLabel58)
                        .addGap(44, 44, 44))))
        );

        frameStudent_btnSearch.setBackground(new java.awt.Color(38, 166, 91));
        frameStudent_btnSearch.setFont(new java.awt.Font("Tahoma", 1, 16)); // NOI18N
        frameStudent_btnSearch.setText("Search");
        frameStudent_btnSearch.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                frameStudent_btnSearchActionPerformed(evt);
            }
        });

        jLabel13.setFont(new java.awt.Font("Tahoma", 1, 16)); // NOI18N
        jLabel13.setText("Enter Student's ID:");

        frameStudent_btnSearchReset.setBackground(new java.awt.Color(38, 166, 91));
        frameStudent_btnSearchReset.setFont(new java.awt.Font("Tahoma", 1, 16)); // NOI18N
        frameStudent_btnSearchReset.setText("Reset");
        frameStudent_btnSearchReset.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                frameStudent_btnSearchResetActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout frameStudent_PAddStudent3Layout = new javax.swing.GroupLayout(frameStudent_PAddStudent3);
        frameStudent_PAddStudent3.setLayout(frameStudent_PAddStudent3Layout);
        frameStudent_PAddStudent3Layout.setHorizontalGroup(
            frameStudent_PAddStudent3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel16, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addGroup(frameStudent_PAddStudent3Layout.createSequentialGroup()
                .addGap(72, 72, 72)
                .addGroup(frameStudent_PAddStudent3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(frameStudent_PAddStudent3Layout.createSequentialGroup()
                        .addComponent(jLabel13, javax.swing.GroupLayout.PREFERRED_SIZE, 166, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(frameStudent_tfEnterPass, javax.swing.GroupLayout.PREFERRED_SIZE, 130, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(frameStudent_btnSearch))
                    .addGroup(frameStudent_PAddStudent3Layout.createSequentialGroup()
                        .addGroup(frameStudent_PAddStudent3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                            .addComponent(jLabel55, javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel56, javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel51, javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel49, javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel48, javax.swing.GroupLayout.Alignment.LEADING))
                        .addGap(12, 12, 12)
                        .addGroup(frameStudent_PAddStudent3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, frameStudent_PAddStudent3Layout.createSequentialGroup()
                                .addComponent(frameStudent_tfCitySearch, javax.swing.GroupLayout.PREFERRED_SIZE, 202, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                .addComponent(jLabel57)
                                .addGap(34, 34, 34)
                                .addComponent(frameStudent_tfPhoneSearch, javax.swing.GroupLayout.PREFERRED_SIZE, 198, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addComponent(frameStudent_tfAddressSearch)
                            .addGroup(frameStudent_PAddStudent3Layout.createSequentialGroup()
                                .addGroup(frameStudent_PAddStudent3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                    .addComponent(frameStudent_tfAgeSearch, javax.swing.GroupLayout.PREFERRED_SIZE, 199, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addGroup(frameStudent_PAddStudent3Layout.createSequentialGroup()
                                        .addComponent(frameStudent_tfIdSearch, javax.swing.GroupLayout.PREFERRED_SIZE, 163, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                        .addComponent(jLabel53))
                                    .addGroup(frameStudent_PAddStudent3Layout.createSequentialGroup()
                                        .addComponent(frameStudent_tfNameSearch)
                                        .addGap(53, 53, 53)
                                        .addGroup(frameStudent_PAddStudent3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                            .addComponent(jLabel52)
                                            .addComponent(jLabel50))))
                                .addGap(28, 28, 28)
                                .addGroup(frameStudent_PAddStudent3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(frameStudent_tfLevelSearch, javax.swing.GroupLayout.PREFERRED_SIZE, 199, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addComponent(frameStudent_tfSearchGender, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addComponent(frameStudent_tfLNSearch))
                                .addGap(0, 7, Short.MAX_VALUE)))
                        .addContainerGap(166, Short.MAX_VALUE))))
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, frameStudent_PAddStudent3Layout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(frameStudent_btnSearchReset))
        );

        frameStudent_PAddStudent3Layout.linkSize(javax.swing.SwingConstants.HORIZONTAL, new java.awt.Component[] {frameStudent_tfAgeSearch, frameStudent_tfCitySearch, frameStudent_tfIdSearch, frameStudent_tfLNSearch, frameStudent_tfLevelSearch, frameStudent_tfNameSearch, frameStudent_tfPhoneSearch, frameStudent_tfSearchGender});

        frameStudent_PAddStudent3Layout.linkSize(javax.swing.SwingConstants.HORIZONTAL, new java.awt.Component[] {frameStudent_btnSearch, frameStudent_btnSearchReset});

        frameStudent_PAddStudent3Layout.setVerticalGroup(
            frameStudent_PAddStudent3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(frameStudent_PAddStudent3Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jPanel16, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGroup(frameStudent_PAddStudent3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(frameStudent_PAddStudent3Layout.createSequentialGroup()
                        .addGap(18, 18, 18)
                        .addComponent(frameStudent_btnSearch)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(frameStudent_btnSearchReset)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 41, Short.MAX_VALUE)
                        .addGroup(frameStudent_PAddStudent3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabel48)
                            .addComponent(frameStudent_tfIdSearch, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jLabel53)
                            .addComponent(frameStudent_tfLevelSearch, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 32, Short.MAX_VALUE)
                        .addGroup(frameStudent_PAddStudent3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(frameStudent_tfLNSearch, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jLabel50)
                            .addComponent(frameStudent_tfNameSearch, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jLabel49))
                        .addGap(34, 34, 34)
                        .addGroup(frameStudent_PAddStudent3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(frameStudent_tfAgeSearch, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jLabel51)
                            .addComponent(jLabel52)
                            .addComponent(frameStudent_tfSearchGender, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 26, Short.MAX_VALUE)
                        .addGroup(frameStudent_PAddStudent3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, frameStudent_PAddStudent3Layout.createSequentialGroup()
                                .addGroup(frameStudent_PAddStudent3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                    .addComponent(jLabel55)
                                    .addComponent(frameStudent_tfAddressSearch, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                                .addGap(54, 54, 54))
                            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, frameStudent_PAddStudent3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                .addComponent(frameStudent_tfCitySearch, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addComponent(jLabel57)
                                .addComponent(frameStudent_tfPhoneSearch, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addComponent(jLabel56)))
                        .addContainerGap(134, Short.MAX_VALUE))
                    .addGroup(frameStudent_PAddStudent3Layout.createSequentialGroup()
                        .addGap(37, 37, 37)
                        .addGroup(frameStudent_PAddStudent3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(frameStudent_tfEnterPass, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jLabel13))
                        .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))))
        );

        javax.swing.GroupLayout jPanel11Layout = new javax.swing.GroupLayout(jPanel11);
        jPanel11.setLayout(jPanel11Layout);
        jPanel11Layout.setHorizontalGroup(
            jPanel11Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 934, Short.MAX_VALUE)
            .addGroup(jPanel11Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addGroup(jPanel11Layout.createSequentialGroup()
                    .addGap(0, 0, Short.MAX_VALUE)
                    .addComponent(frameStudent_PAddStudent3, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGap(0, 0, Short.MAX_VALUE)))
        );
        jPanel11Layout.setVerticalGroup(
            jPanel11Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 699, Short.MAX_VALUE)
            .addGroup(jPanel11Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addGroup(jPanel11Layout.createSequentialGroup()
                    .addGap(0, 0, Short.MAX_VALUE)
                    .addComponent(frameStudent_PAddStudent3, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGap(0, 0, Short.MAX_VALUE)))
        );

        frameStudent_pModificationS.addTab("Search Student", jPanel11);

        jPanel15.setBackground(new java.awt.Color(200, 247, 197));

        tbl_ViewStudent.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null, null}
            },
            new String [] {
                "ID", "First Name", "Last Name", "Age", "Gender", "Class Level", "Address", "City", "Phone"
            }
        ) {
            boolean[] canEdit = new boolean [] {
                false, false, false, false, false, false, false, false, false
            };

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        jScrollPane1.setViewportView(tbl_ViewStudent);

        javax.swing.GroupLayout jPanel15Layout = new javax.swing.GroupLayout(jPanel15);
        jPanel15.setLayout(jPanel15Layout);
        jPanel15Layout.setHorizontalGroup(
            jPanel15Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jScrollPane1)
        );
        jPanel15Layout.setVerticalGroup(
            jPanel15Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel15Layout.createSequentialGroup()
                .addGap(44, 44, 44)
                .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 527, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(128, Short.MAX_VALUE))
        );

        frameStudent_pModificationS.addTab("View Student", jPanel15);

        javax.swing.GroupLayout jPanel2Layout = new javax.swing.GroupLayout(jPanel2);
        jPanel2.setLayout(jPanel2Layout);
        jPanel2Layout.setHorizontalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(frameStudent_pModificationS)
        );
        jPanel2Layout.setVerticalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(frameStudent_pModificationS)
        );

        javax.swing.GroupLayout frameStudent_panelBodyLayout = new javax.swing.GroupLayout(frameStudent_panelBody);
        frameStudent_panelBody.setLayout(frameStudent_panelBodyLayout);
        frameStudent_panelBodyLayout.setHorizontalGroup(
            frameStudent_panelBodyLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        frameStudent_panelBodyLayout.setVerticalGroup(
            frameStudent_panelBodyLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );

        javax.swing.GroupLayout frameStudentLayout = new javax.swing.GroupLayout(frameStudent.getContentPane());
        frameStudent.getContentPane().setLayout(frameStudentLayout);
        frameStudentLayout.setHorizontalGroup(
            frameStudentLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(frameStudent_panelBody, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        frameStudentLayout.setVerticalGroup(
            frameStudentLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(frameStudent_panelBody, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );

        javax.swing.GroupLayout jPanel4Layout = new javax.swing.GroupLayout(jPanel4);
        jPanel4.setLayout(jPanel4Layout);
        jPanel4Layout.setHorizontalGroup(
            jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 100, Short.MAX_VALUE)
        );
        jPanel4Layout.setVerticalGroup(
            jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 100, Short.MAX_VALUE)
        );

        jLabel1.setText("jLabel1");

        jLabel19.setText("jLabel19");

        javax.swing.GroupLayout jPanel6Layout = new javax.swing.GroupLayout(jPanel6);
        jPanel6.setLayout(jPanel6Layout);
        jPanel6Layout.setHorizontalGroup(
            jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 100, Short.MAX_VALUE)
        );
        jPanel6Layout.setVerticalGroup(
            jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 100, Short.MAX_VALUE)
        );

        jLabel20.setText("jLabel20");

        jLabel21.setText("jLabel21");

        jLabel22.setText("jLabel22");

        jLabel23.setText("jLabel23");

        javax.swing.GroupLayout jPanel8Layout = new javax.swing.GroupLayout(jPanel8);
        jPanel8.setLayout(jPanel8Layout);
        jPanel8Layout.setHorizontalGroup(
            jPanel8Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 100, Short.MAX_VALUE)
        );
        jPanel8Layout.setVerticalGroup(
            jPanel8Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 100, Short.MAX_VALUE)
        );

        javax.swing.GroupLayout jPanel9Layout = new javax.swing.GroupLayout(jPanel9);
        jPanel9.setLayout(jPanel9Layout);
        jPanel9Layout.setHorizontalGroup(
            jPanel9Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 100, Short.MAX_VALUE)
        );
        jPanel9Layout.setVerticalGroup(
            jPanel9Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 100, Short.MAX_VALUE)
        );

        javax.swing.GroupLayout jPanel13Layout = new javax.swing.GroupLayout(jPanel13);
        jPanel13.setLayout(jPanel13Layout);
        jPanel13Layout.setHorizontalGroup(
            jPanel13Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 100, Short.MAX_VALUE)
        );
        jPanel13Layout.setVerticalGroup(
            jPanel13Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 100, Short.MAX_VALUE)
        );

        javax.swing.GroupLayout jPanel14Layout = new javax.swing.GroupLayout(jPanel14);
        jPanel14.setLayout(jPanel14Layout);
        jPanel14Layout.setHorizontalGroup(
            jPanel14Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 100, Short.MAX_VALUE)
        );
        jPanel14Layout.setVerticalGroup(
            jPanel14Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 100, Short.MAX_VALUE)
        );

        javax.swing.GroupLayout jPanel3Layout = new javax.swing.GroupLayout(jPanel3);
        jPanel3.setLayout(jPanel3Layout);
        jPanel3Layout.setHorizontalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 100, Short.MAX_VALUE)
        );
        jPanel3Layout.setVerticalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 100, Short.MAX_VALUE)
        );

        frameTeachers.setBackground(new java.awt.Color(200, 247, 197));
        frameTeachers.setLocationByPlatform(true);
        frameTeachers.setUndecorated(true);

        frameTeachers_panelBody.setBackground(new java.awt.Color(200, 247, 197));

        jPanel17.setBackground(new java.awt.Color(38, 166, 91));

        jLabel61.setFont(new java.awt.Font("Tahoma", 1, 36)); // NOI18N
        jLabel61.setText("Teachers Profile");

        jLabel62.setIcon(new javax.swing.ImageIcon(getClass().getResource("/schoolManagement/images/icons8_Classroom_96px_2.png"))); // NOI18N
        jLabel62.setText("\n");

        jLabel64.setFont(new java.awt.Font("Tahoma", 1, 18)); // NOI18N
        jLabel64.setText("-");
        jLabel64.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mousePressed(java.awt.event.MouseEvent evt) {
                jLabel64MousePressed(evt);
            }
        });

        jLabel65.setFont(new java.awt.Font("Tahoma", 1, 18)); // NOI18N
        jLabel65.setText("X");
        jLabel65.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mousePressed(java.awt.event.MouseEvent evt) {
                jLabel65MousePressed(evt);
            }
        });

        javax.swing.GroupLayout jPanel17Layout = new javax.swing.GroupLayout(jPanel17);
        jPanel17.setLayout(jPanel17Layout);
        jPanel17Layout.setHorizontalGroup(
            jPanel17Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel17Layout.createSequentialGroup()
                .addGap(39, 39, 39)
                .addComponent(jLabel62, javax.swing.GroupLayout.PREFERRED_SIZE, 106, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jLabel61)
                .addContainerGap(283, Short.MAX_VALUE))
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel17Layout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(jLabel64, javax.swing.GroupLayout.PREFERRED_SIZE, 41, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jLabel65, javax.swing.GroupLayout.PREFERRED_SIZE, 37, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(10, 10, 10))
        );
        jPanel17Layout.setVerticalGroup(
            jPanel17Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel17Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel17Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel64, javax.swing.GroupLayout.PREFERRED_SIZE, 32, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel65, javax.swing.GroupLayout.PREFERRED_SIZE, 32, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGroup(jPanel17Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel17Layout.createSequentialGroup()
                        .addComponent(jLabel62, javax.swing.GroupLayout.PREFERRED_SIZE, 116, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addContainerGap())
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel17Layout.createSequentialGroup()
                        .addComponent(jLabel61)
                        .addGap(44, 44, 44))))
        );

        frameTeacher_lstTeacher.setModel(teacherListModel);
        frameTeacher_lstTeacher.setSelectionMode(javax.swing.ListSelectionModel.SINGLE_SELECTION);
        jScrollPane2.setViewportView(frameTeacher_lstTeacher);

        javax.swing.GroupLayout frameTeachers_panelBodyLayout = new javax.swing.GroupLayout(frameTeachers_panelBody);
        frameTeachers_panelBody.setLayout(frameTeachers_panelBodyLayout);
        frameTeachers_panelBodyLayout.setHorizontalGroup(
            frameTeachers_panelBodyLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel17, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addGroup(frameTeachers_panelBodyLayout.createSequentialGroup()
                .addGap(92, 92, 92)
                .addComponent(jScrollPane2, javax.swing.GroupLayout.PREFERRED_SIZE, 486, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        frameTeachers_panelBodyLayout.setVerticalGroup(
            frameTeachers_panelBodyLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(frameTeachers_panelBodyLayout.createSequentialGroup()
                .addComponent(jPanel17, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(76, 76, 76)
                .addComponent(jScrollPane2, javax.swing.GroupLayout.PREFERRED_SIZE, 241, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(0, 206, Short.MAX_VALUE))
        );

        javax.swing.GroupLayout frameTeachersLayout = new javax.swing.GroupLayout(frameTeachers.getContentPane());
        frameTeachers.getContentPane().setLayout(frameTeachersLayout);
        frameTeachersLayout.setHorizontalGroup(
            frameTeachersLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(frameTeachers_panelBody, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        frameTeachersLayout.setVerticalGroup(
            frameTeachersLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(frameTeachers_panelBody, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );

        jLabel63.setText("jLabel63");

        jLabel71.setText("jLabel71");

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setTitle("School Management System");
        setAutoRequestFocus(false);
        setFont(new java.awt.Font("Aldhabi", 1, 12)); // NOI18N
        setForeground(new java.awt.Color(255, 204, 102));
        setLocationByPlatform(true);
        setUndecorated(true);

        panelBack.setBackground(new java.awt.Color(200, 247, 197));

        panelSideLeft.setBackground(new java.awt.Color(38, 166, 91));

        btn_schoolAdmission.setBackground(new java.awt.Color(200, 247, 197));
        btn_schoolAdmission.setFont(new java.awt.Font("Tahoma", 1, 18)); // NOI18N
        btn_schoolAdmission.setText("School Admission");
        btn_schoolAdmission.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                btn_schoolAdmissionMouseEntered(evt);
            }
            public void mouseExited(java.awt.event.MouseEvent evt) {
                btn_schoolAdmissionMouseExited(evt);
            }
        });

        btn_schoolFee1.setBackground(new java.awt.Color(200, 247, 197));
        btn_schoolFee1.setFont(new java.awt.Font("Tahoma", 1, 18)); // NOI18N
        btn_schoolFee1.setText("School Fee Structure");
        btn_schoolFee1.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                btn_schoolFee1MouseEntered(evt);
            }
            public void mouseExited(java.awt.event.MouseEvent evt) {
                btn_schoolFee1MouseExited(evt);
            }
        });

        javax.swing.GroupLayout panelSideLeftLayout = new javax.swing.GroupLayout(panelSideLeft);
        panelSideLeft.setLayout(panelSideLeftLayout);
        panelSideLeftLayout.setHorizontalGroup(
            panelSideLeftLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(panelSideLeftLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(panelSideLeftLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(btn_schoolAdmission, javax.swing.GroupLayout.DEFAULT_SIZE, 294, Short.MAX_VALUE)
                    .addComponent(btn_schoolFee1, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, 294, Short.MAX_VALUE))
                .addContainerGap())
        );
        panelSideLeftLayout.setVerticalGroup(
            panelSideLeftLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, panelSideLeftLayout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(btn_schoolFee1, javax.swing.GroupLayout.PREFERRED_SIZE, 47, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(btn_schoolAdmission, javax.swing.GroupLayout.PREFERRED_SIZE, 47, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(122, 122, 122))
        );

        jPanel1.setBackground(new java.awt.Color(38, 166, 91));

        jLabel10.setFont(new java.awt.Font("Tahoma", 1, 24)); // NOI18N
        jLabel10.setText("School Management System");

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addGap(247, 247, 247)
                .addComponent(jLabel10)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addGap(54, 54, 54)
                .addComponent(jLabel10)
                .addContainerGap(60, Short.MAX_VALUE))
        );

        lblExit.setFont(new java.awt.Font("Tahoma", 1, 24)); // NOI18N
        lblExit.setText("X");
        lblExit.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mousePressed(java.awt.event.MouseEvent evt) {
                lblExitMousePressed(evt);
            }
        });

        lblMin.setFont(new java.awt.Font("Tahoma", 1, 24)); // NOI18N
        lblMin.setText("-");
        lblMin.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mousePressed(java.awt.event.MouseEvent evt) {
                lblMinMousePressed(evt);
            }
        });

        btnTeachers.setBackground(new java.awt.Color(200, 247, 197));
        btnTeachers.setIcon(new javax.swing.ImageIcon(getClass().getResource("/schoolManagement/images/icons8_Classroom_96px_2.png"))); // NOI18N
        btnTeachers.setEnabled(false);
        btnTeachers.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                btnTeachersMouseEntered(evt);
            }
            public void mouseExited(java.awt.event.MouseEvent evt) {
                btnTeachersMouseExited(evt);
            }
        });
        btnTeachers.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnTeachersActionPerformed(evt);
            }
        });

        btnStudents.setBackground(new java.awt.Color(200, 247, 197));
        btnStudents.setIcon(new javax.swing.ImageIcon(getClass().getResource("/schoolManagement/images/icons8_Students_100px_1.png"))); // NOI18N
        btnStudents.setEnabled(false);
        btnStudents.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                btnStudentsMouseEntered(evt);
            }
            public void mouseExited(java.awt.event.MouseEvent evt) {
                btnStudentsMouseExited(evt);
            }
        });
        btnStudents.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnStudentsActionPerformed(evt);
            }
        });

        btnLogin.setBackground(new java.awt.Color(200, 247, 197));
        btnLogin.setIcon(new javax.swing.ImageIcon(getClass().getResource("/schoolManagement/images/icons8_Access_100px.png"))); // NOI18N
        btnLogin.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                btnLoginMouseClicked(evt);
            }
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                btnLoginMouseEntered(evt);
            }
            public void mouseExited(java.awt.event.MouseEvent evt) {
                btnLoginMouseExited(evt);
            }
        });
        btnLogin.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnLoginActionPerformed(evt);
            }
        });

        jLabel7.setFont(new java.awt.Font("Tahoma", 1, 24)); // NOI18N
        jLabel7.setText("Login");

        jLabel9.setFont(new java.awt.Font("Tahoma", 1, 24)); // NOI18N
        jLabel9.setText("Students");

        jLabel2.setFont(new java.awt.Font("Tahoma", 1, 24)); // NOI18N
        jLabel2.setText("Teachers");

        javax.swing.GroupLayout panelBackLayout = new javax.swing.GroupLayout(panelBack);
        panelBack.setLayout(panelBackLayout);
        panelBackLayout.setHorizontalGroup(
            panelBackLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(panelBackLayout.createSequentialGroup()
                .addComponent(panelSideLeft, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGroup(panelBackLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addGroup(javax.swing.GroupLayout.Alignment.LEADING, panelBackLayout.createSequentialGroup()
                        .addGap(1, 1, 1)
                        .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                    .addGroup(javax.swing.GroupLayout.Alignment.LEADING, panelBackLayout.createSequentialGroup()
                        .addGap(153, 153, 153)
                        .addComponent(jLabel7)
                        .addGap(148, 148, 148)
                        .addComponent(jLabel9, javax.swing.GroupLayout.PREFERRED_SIZE, 114, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(107, 107, 107)
                        .addComponent(jLabel2, javax.swing.GroupLayout.DEFAULT_SIZE, 134, Short.MAX_VALUE)
                        .addGap(137, 137, 137))
                    .addGroup(panelBackLayout.createSequentialGroup()
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(lblMin, javax.swing.GroupLayout.PREFERRED_SIZE, 41, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(lblExit, javax.swing.GroupLayout.PREFERRED_SIZE, 41, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(panelBackLayout.createSequentialGroup()
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(btnLogin, javax.swing.GroupLayout.PREFERRED_SIZE, 158, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(45, 45, 45)
                        .addComponent(btnStudents, javax.swing.GroupLayout.PREFERRED_SIZE, 158, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(61, 61, 61)
                        .addComponent(btnTeachers, javax.swing.GroupLayout.PREFERRED_SIZE, 179, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))))
        );

        panelBackLayout.linkSize(javax.swing.SwingConstants.HORIZONTAL, new java.awt.Component[] {btnLogin, btnStudents, btnTeachers});

        panelBackLayout.setVerticalGroup(
            panelBackLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(panelSideLeft, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addGroup(panelBackLayout.createSequentialGroup()
                .addGroup(panelBackLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(lblExit)
                    .addComponent(lblMin))
                .addGap(97, 97, 97)
                .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(67, 67, 67)
                .addGroup(panelBackLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(btnLogin, javax.swing.GroupLayout.PREFERRED_SIZE, 179, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(btnTeachers, javax.swing.GroupLayout.PREFERRED_SIZE, 187, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(btnStudents, javax.swing.GroupLayout.PREFERRED_SIZE, 179, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(panelBackLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel7)
                    .addComponent(jLabel2)
                    .addComponent(jLabel9))
                .addGap(84, 269, Short.MAX_VALUE))
        );

        panelBackLayout.linkSize(javax.swing.SwingConstants.VERTICAL, new java.awt.Component[] {btnLogin, btnStudents, btnTeachers});

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(panelBack, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(panelBack, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );

        pack();
        setLocationRelativeTo(null);
    }// </editor-fold>//GEN-END:initComponents

    private void lblExitMousePressed(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_lblExitMousePressed
        System.exit(0);
    }//GEN-LAST:event_lblExitMousePressed

    private void framePassWord_XMousePressed(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_framePassWord_XMousePressed
        framePassWord.setVisible(false);
    }//GEN-LAST:event_framePassWord_XMousePressed

    private void framePassWord_minMousePressed(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_framePassWord_minMousePressed
        framePassWord.setState(framePassWord.ICONIFIED);
    }//GEN-LAST:event_framePassWord_minMousePressed

    private void lblMinMousePressed(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_lblMinMousePressed
        this.setState(JFrame.ICONIFIED);
    }//GEN-LAST:event_lblMinMousePressed

    private void framePassWord_btnLoginActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_framePassWord_btnLoginActionPerformed

        String textUser = framePassWord_tfUser.getText();
        String pass = framePassWord_tfPass.getText();
        //Regex For validation of UserName and PassWord
        try {
            if (!pass.matches("^(?=.*[0-9])(?=.*[a-z])(?=.*[A-Z])(?=.*[@#$%^&+=])(?=\\S+$).{8,}$") || !textUser.matches("^[A][A-Z]{4}$")) {
                throw new IllegalArgumentException("PAssword must be like:2aa@$AAA\n User Not correct");
            } else if (textUser.equals("ADMIN") && (pass.equals("2aa@$AAA"))) {
                btnStudents.setEnabled(true);
                btnTeachers.setEnabled(true);
                framePassWord.dispose();
                framePassWord_tfPass.setText("");
                framePassWord_tfUser.setText("");
            }
        } catch (IllegalArgumentException e) {
            JOptionPane.showMessageDialog(this,
                    e.getMessage(),
                    "Error in username or password",
                    JOptionPane.ERROR_MESSAGE);
        }


    }//GEN-LAST:event_framePassWord_btnLoginActionPerformed
    //Set background color when mouse entered
    private void btnLoginMouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btnLoginMouseEntered
        btnLogin.setBackground(new java.awt.Color(255, 247, 205));
    }//GEN-LAST:event_btnLoginMouseEntered
    //Set background color when mouse exit
    private void btnLoginMouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btnLoginMouseExited
        btnLogin.setBackground(new java.awt.Color(200, 247, 197));
    }//GEN-LAST:event_btnLoginMouseExited
    //Set background color when mouse entered
    private void btnStudentsMouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btnStudentsMouseEntered
        btnStudents.setBackground(new java.awt.Color(255, 247, 205));
    }//GEN-LAST:event_btnStudentsMouseEntered
    //Set background color when mouse exit
    private void btnStudentsMouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btnStudentsMouseExited
        btnStudents.setBackground(new java.awt.Color(200, 247, 197));
    }//GEN-LAST:event_btnStudentsMouseExited
    //Set background color when mouse entered
    private void btnTeachersMouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btnTeachersMouseEntered
        btnTeachers.setBackground(new java.awt.Color(255, 247, 205));
    }//GEN-LAST:event_btnTeachersMouseEntered
    //Set background color when mouse exit
    private void btnTeachersMouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btnTeachersMouseExited
        btnTeachers.setBackground(new java.awt.Color(200, 247, 197));
    }//GEN-LAST:event_btnTeachersMouseExited
    //Set background color when mouse entered
    private void btn_schoolAdmissionMouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btn_schoolAdmissionMouseEntered
        btn_schoolAdmission.setBackground(new java.awt.Color(255, 247, 205));
    }//GEN-LAST:event_btn_schoolAdmissionMouseEntered
    //Set background color when mouse exit
    private void btn_schoolAdmissionMouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btn_schoolAdmissionMouseExited
        btn_schoolAdmission.setBackground(new java.awt.Color(200, 247, 197));
    }//GEN-LAST:event_btn_schoolAdmissionMouseExited
    //Set background color when mouse entered
    private void btn_schoolFee1MouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btn_schoolFee1MouseEntered
        btn_schoolFee1.setBackground(new java.awt.Color(255, 247, 205));
    }//GEN-LAST:event_btn_schoolFee1MouseEntered
    //Set background color when mouse exit
    private void btn_schoolFee1MouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btn_schoolFee1MouseExited
        btn_schoolFee1.setBackground(new java.awt.Color(200, 247, 197));
    }//GEN-LAST:event_btn_schoolFee1MouseExited
    //Jframe passWord will be appear
    private void btnLoginMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btnLoginMouseClicked
        framePassWord.setVisible(true);
        framePassWord.pack();
    }//GEN-LAST:event_btnLoginMouseClicked

    private void btnLoginActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnLoginActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_btnLoginActionPerformed
    //frame Student will be appear after clicking on this button
    private void btnStudentsActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnStudentsActionPerformed
        frameStudent.setVisible(true);
        frameStudent.pack();
    }//GEN-LAST:event_btnStudentsActionPerformed

    private void frameStudent_tfAgeDelActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_frameStudent_tfAgeDelActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_frameStudent_tfAgeDelActionPerformed

    private void frameStudent_btnAddActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_frameStudent_btnAddActionPerformed
        String firstName = frameStudent_tfName.getText();
        if (firstName.length() < 1 || firstName.length() > 100) {
            JOptionPane.showMessageDialog(this,
                    "firstname must be in 1 to 100",
                    "Input error, firstname invalid",
                    JOptionPane.ERROR_MESSAGE);
            return;
        }
        String lastName = frameStudent_tfLastName.getText();
        if (lastName.length() < 1 || lastName.length() > 50) {
            JOptionPane.showMessageDialog(this,
                    "lastName must be in 1 to 50",
                    "Input error, lastName invalid",
                    JOptionPane.ERROR_MESSAGE);
            return;
        }
        String ageStr = frameStudent_tfAge.getText();
        int age = Integer.parseInt(ageStr);
        if (age < 18 || age > 70) {
            JOptionPane.showMessageDialog(this,
                    "age must be in 18 to 70",
                    "Input error, age invalid",
                    JOptionPane.ERROR_MESSAGE);
            return;
        }

        String gender = (String) frameStudent_cbGender.getSelectedItem();
        String classLevel = frameStudent_tfLevel.getText();
        String address = frameStudent_tfAddress.getText();
        String city = frameStudent_tfCity.getText();
        String phone = frameStudent_tfPhone.getText();

        try {
            if (!phone.matches("^(\\+\\d{1,2}\\s)?\\(?\\d{3}\\)?[\\s.-]\\d{3}[\\s.-]\\d{4}$")) {
                JOptionPane.showMessageDialog(this,
                        "phone number formar must be (xxx-xxx-xxxx)",
                        "Input error,phone number invalid",
                        JOptionPane.ERROR_MESSAGE);
                return;
            }
        } catch (HeadlessException e) {
            JOptionPane.showMessageDialog(this,
                    e.getMessage(),
                    "Input error, phone No. invalid",
                    JOptionPane.ERROR_MESSAGE);
            return;
        }
        Students st = new Students();
        st.firstName = firstName;
        st.lastName = lastName;
        st.age = age;
        st.gender = gender;
        st.classLecvel = classLevel;
        st.address = address;
        st.city = city;
        st.phone = phone;
        try {
            db.addStudent(st);
            JOptionPane.showMessageDialog(this, "Record correctly enter");
        } catch (SQLException e) {
            JOptionPane.showMessageDialog(this,
                    e.getMessage(),
                    "Database access error",
                    JOptionPane.ERROR_MESSAGE);
            return;
        }
    }//GEN-LAST:event_frameStudent_btnAddActionPerformed

    private void frameStudent_btnDelete1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_frameStudent_btnDelete1ActionPerformed
        //This Section is about finding ID specific for deleting
        ResultSet rs;
        String stId = frameStudent_tfDeleteStu.getText();
        long id = Long.parseLong(stId);

        rs = db.findStudent(id);
        //This section is about Deleting the ID specific
        if (id == 0) {
            return;
        }
        try {
            if (rs.next()) {
                frameStudent_tfIdDelete.setText(rs.getLong("ID") + "");
                frameStudent_tfNameDel.setText(rs.getString("firstname"));
                frameStudent_tfLastNameDel.setText((rs.getString("lastname")));
                frameStudent_tfAgeDel.setText(rs.getInt("age") + "");
                frameStudent_tfGenderDel.setText(rs.getString("gender"));
                frameStudent_tfLevelDel.setText(rs.getString("classlevel"));
                frameStudent_tfAddressDel.setText(rs.getString("address"));
                frameStudent_tfCityDel.setText(rs.getString("city"));
                frameStudent_tfPhoneDelete.setText(rs.getString("phone"));
            } else {
                JOptionPane.showMessageDialog(null, "NO Data for this ID");
            }
        } catch (HeadlessException | SQLException e) {
            JOptionPane.showMessageDialog(null, e.getMessage());
        }

        int decision = JOptionPane.showConfirmDialog(
                this,
                "Are you sure you want to delete the following record?\n" + stId,
                "Confirm deletion",
                JOptionPane.YES_NO_OPTION);
        if (decision == JOptionPane.YES_OPTION) {
            try {
                db.deleteStudent(id);
            } catch (SQLException e) {
                JOptionPane.showMessageDialog(this,
                        e.getMessage(),
                        "Database access error",
                        JOptionPane.ERROR_MESSAGE);
            }
        }

    }//GEN-LAST:event_frameStudent_btnDelete1ActionPerformed

    private void frameStudent_btnSearchActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_frameStudent_btnSearchActionPerformed

        ResultSet rs;
        String stId = frameStudent_tfEnterPass.getText();
        long id = Long.parseLong(stId);

        rs = db.findStudent(id);
        try {
            if (rs.next()) {
                frameStudent_tfIdSearch.setText(rs.getLong("ID") + "");
                frameStudent_tfNameSearch.setText(rs.getString("firstname"));
                frameStudent_tfLNSearch.setText((rs.getString("lastname")));
                frameStudent_tfAgeSearch.setText(rs.getInt("age") + "");
                frameStudent_tfSearchGender.setText(rs.getString("gender"));
                frameStudent_tfLevelSearch.setText(rs.getString("classlevel"));
                frameStudent_tfAddressSearch.setText(rs.getString("address"));
                frameStudent_tfCitySearch.setText(rs.getString("city"));
                frameStudent_tfPhoneSearch.setText(rs.getString("phone"));
            } else {
                JOptionPane.showMessageDialog(null, "NO Data for this ID");
            }
        } catch (HeadlessException | SQLException e) {
            JOptionPane.showMessageDialog(null, e.getMessage());
        }


    }//GEN-LAST:event_frameStudent_btnSearchActionPerformed

    private void frameStudent_btnSearchResetActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_frameStudent_btnSearchResetActionPerformed
        frameStudent_tfIdSearch.setText("");
        frameStudent_tfNameSearch.setText("");
        frameStudent_tfLNSearch.setText("");
        frameStudent_tfAgeSearch.setText("");
        frameStudent_tfSearchGender.setText("");
        frameStudent_tfLevelSearch.setText("");
        frameStudent_tfAddressSearch.setText("");
        frameStudent_tfCitySearch.setText("");
        frameStudent_tfPhoneSearch.setText("");
        frameStudent_tfEnterPass.setText("");
    }//GEN-LAST:event_frameStudent_btnSearchResetActionPerformed

    private void frameStudent_btnUpdateActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_frameStudent_btnUpdateActionPerformed

        String firstName = frameStudent_tfNameUpdate.getText();

        if (firstName.length() < 1 || firstName.length() > 100) {
            JOptionPane.showMessageDialog(this,
                    "firstname must be in 1 to 100",
                    "Input error, firstname invalid",
                    JOptionPane.ERROR_MESSAGE);
            return;
        }
        String lastName = frameStudent_tfLastNameUpdate.getText();
        if (lastName.length() < 1 || lastName.length() > 50) {
            JOptionPane.showMessageDialog(this,
                    "lastName must be in 1 to 50",
                    "Input error, lastName invalid",
                    JOptionPane.ERROR_MESSAGE);
            return;
        }
        String ageStr = frameStudent_tfAgeUpdate.getText();
        int age = Integer.parseInt(ageStr);
        if (age < 18 || age > 70) {
            JOptionPane.showMessageDialog(this,
                    "age must be in 18 to 70",
                    "Input error, age invalid",
                    JOptionPane.ERROR_MESSAGE);
            return;
        }

        String gender = (String) frameStudent_cbGenderUpdate.getSelectedItem();
        String classLevel = frameStudent_tfLevelUpdate.getText();
        String address = frameStudent_tfAddressUpdate.getText();
        String city = frameStudent_tfCityUpdate.getText();
        String phone = frameStudent_tfPhoneUpdate.getText();
        String strId = frameStudent_tfIdUpdate.getText();
        long id = Long.parseLong(strId);

        try {
            if (!phone.matches("^(\\+\\d{1,2}\\s)?\\(?\\d{3}\\)?[\\s.-]\\d{3}[\\s.-]\\d{4}$")) {
                JOptionPane.showMessageDialog(this,
                        "phone number formar must be (xxx-xxx-xxxx)",
                        "Input error,phone number invalid",
                        JOptionPane.ERROR_MESSAGE);
                return;
            }
        } catch (HeadlessException e) {
            JOptionPane.showMessageDialog(this,
                    e.getMessage(),
                    "Input error, phone No. invalid",
                    JOptionPane.ERROR_MESSAGE);
            return;
        }
        Students st = new Students();
        st.id = id;
        st.firstName = firstName;
        st.lastName = lastName;
        st.age = age;
        st.gender = gender;
        st.classLecvel = classLevel;
        st.address = address;
        st.city = city;
        st.phone = phone;
        try {
            db.updateStudent(st);
            JOptionPane.showMessageDialog(null, "Data COrrectly Update");
        } catch (SQLException ex) {
            JOptionPane.showMessageDialog(this,
                    ex.getMessage(),
                    "Database access error",
                    JOptionPane.ERROR_MESSAGE);
        }


    }//GEN-LAST:event_frameStudent_btnUpdateActionPerformed

    private void btnTeachersActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnTeachersActionPerformed
        frameTeachers.setVisible(true);
        frameTeachers.pack();
    }//GEN-LAST:event_btnTeachersActionPerformed

    private void jLabel65MousePressed(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jLabel65MousePressed
        frameTeachers.setVisible(false);
    }//GEN-LAST:event_jLabel65MousePressed

    private void jLabel64MousePressed(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jLabel64MousePressed
        frameTeachers.setState(frameTeachers.ICONIFIED);
    }//GEN-LAST:event_jLabel64MousePressed

    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(SchoolManagementSys.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(SchoolManagementSys.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(SchoolManagementSys.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(SchoolManagementSys.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new SchoolManagementSys().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnLogin;
    private javax.swing.JButton btnStudents;
    private javax.swing.JButton btnTeachers;
    private javax.swing.JButton btn_schoolAdmission;
    private javax.swing.JButton btn_schoolFee1;
    private javax.swing.JFrame framePassWord;
    private javax.swing.JLabel framePassWord_X;
    private javax.swing.JButton framePassWord_btnLogin;
    private javax.swing.JLabel framePassWord_lblLogin;
    private javax.swing.JLabel framePassWord_lblPass;
    private javax.swing.JLabel framePassWord_lblUser;
    private javax.swing.JLabel framePassWord_min;
    private javax.swing.JPanel framePassWord_panelTop;
    private javax.swing.JPasswordField framePassWord_tfPass;
    private javax.swing.JTextField framePassWord_tfUser;
    private javax.swing.JPanel framePassword_panelBody;
    private javax.swing.JFrame frameStudent;
    private javax.swing.JPanel frameStudent_PAddStudent;
    private javax.swing.JPanel frameStudent_PAddStudent3;
    private javax.swing.JButton frameStudent_btnAdd;
    private javax.swing.JButton frameStudent_btnDelete1;
    private javax.swing.JButton frameStudent_btnSearch;
    private javax.swing.JButton frameStudent_btnSearchReset;
    private javax.swing.JButton frameStudent_btnUpdate;
    private javax.swing.JComboBox<String> frameStudent_cbGender;
    private javax.swing.JComboBox<String> frameStudent_cbGenderUpdate;
    private javax.swing.JPanel frameStudent_pDelete;
    private javax.swing.JTabbedPane frameStudent_pModificationS;
    private javax.swing.JPanel frameStudent_pUpdate;
    private javax.swing.JPanel frameStudent_panelBody;
    private javax.swing.JTextField frameStudent_tfAddress;
    private javax.swing.JTextField frameStudent_tfAddressDel;
    private javax.swing.JTextField frameStudent_tfAddressSearch;
    private javax.swing.JTextField frameStudent_tfAddressUpdate;
    private javax.swing.JTextField frameStudent_tfAge;
    private javax.swing.JTextField frameStudent_tfAgeDel;
    private javax.swing.JTextField frameStudent_tfAgeSearch;
    private javax.swing.JTextField frameStudent_tfAgeUpdate;
    private javax.swing.JTextField frameStudent_tfCity;
    private javax.swing.JTextField frameStudent_tfCityDel;
    private javax.swing.JTextField frameStudent_tfCitySearch;
    private javax.swing.JTextField frameStudent_tfCityUpdate;
    private javax.swing.JTextField frameStudent_tfDeleteStu;
    private javax.swing.JTextField frameStudent_tfEnterPass;
    private javax.swing.JTextField frameStudent_tfGenderDel;
    private javax.swing.JTextField frameStudent_tfId;
    private javax.swing.JTextField frameStudent_tfIdDelete;
    private javax.swing.JTextField frameStudent_tfIdSearch;
    private javax.swing.JTextField frameStudent_tfIdUpdate;
    private javax.swing.JTextField frameStudent_tfLNSearch;
    private javax.swing.JTextField frameStudent_tfLastName;
    private javax.swing.JTextField frameStudent_tfLastNameDel;
    private javax.swing.JTextField frameStudent_tfLastNameUpdate;
    private javax.swing.JTextField frameStudent_tfLevel;
    private javax.swing.JTextField frameStudent_tfLevelDel;
    private javax.swing.JTextField frameStudent_tfLevelSearch;
    private javax.swing.JTextField frameStudent_tfLevelUpdate;
    private javax.swing.JTextField frameStudent_tfName;
    private javax.swing.JTextField frameStudent_tfNameDel;
    private javax.swing.JTextField frameStudent_tfNameSearch;
    private javax.swing.JTextField frameStudent_tfNameUpdate;
    private javax.swing.JTextField frameStudent_tfPhone;
    private javax.swing.JTextField frameStudent_tfPhoneDelete;
    private javax.swing.JTextField frameStudent_tfPhoneSearch;
    private javax.swing.JTextField frameStudent_tfPhoneUpdate;
    private javax.swing.JTextField frameStudent_tfSearchGender;
    private javax.swing.JTextField frameStudent_tfUpdateEnterId;
    private javax.swing.JTabbedPane frameStudent_tpDelete;
    private javax.swing.JList<Teachers> frameTeacher_lstTeacher;
    private javax.swing.JFrame frameTeachers;
    private javax.swing.JPanel frameTeachers_panelBody;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel10;
    private javax.swing.JLabel jLabel11;
    private javax.swing.JLabel jLabel12;
    private javax.swing.JLabel jLabel13;
    private javax.swing.JLabel jLabel14;
    private javax.swing.JLabel jLabel15;
    private javax.swing.JLabel jLabel16;
    private javax.swing.JLabel jLabel17;
    private javax.swing.JLabel jLabel18;
    private javax.swing.JLabel jLabel19;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel20;
    private javax.swing.JLabel jLabel21;
    private javax.swing.JLabel jLabel22;
    private javax.swing.JLabel jLabel23;
    private javax.swing.JLabel jLabel24;
    private javax.swing.JLabel jLabel25;
    private javax.swing.JLabel jLabel26;
    private javax.swing.JLabel jLabel27;
    private javax.swing.JLabel jLabel28;
    private javax.swing.JLabel jLabel29;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel30;
    private javax.swing.JLabel jLabel31;
    private javax.swing.JLabel jLabel32;
    private javax.swing.JLabel jLabel33;
    private javax.swing.JLabel jLabel34;
    private javax.swing.JLabel jLabel35;
    private javax.swing.JLabel jLabel36;
    private javax.swing.JLabel jLabel37;
    private javax.swing.JLabel jLabel38;
    private javax.swing.JLabel jLabel39;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel40;
    private javax.swing.JLabel jLabel41;
    private javax.swing.JLabel jLabel42;
    private javax.swing.JLabel jLabel43;
    private javax.swing.JLabel jLabel44;
    private javax.swing.JLabel jLabel45;
    private javax.swing.JLabel jLabel46;
    private javax.swing.JLabel jLabel47;
    private javax.swing.JLabel jLabel48;
    private javax.swing.JLabel jLabel49;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel50;
    private javax.swing.JLabel jLabel51;
    private javax.swing.JLabel jLabel52;
    private javax.swing.JLabel jLabel53;
    private javax.swing.JLabel jLabel54;
    private javax.swing.JLabel jLabel55;
    private javax.swing.JLabel jLabel56;
    private javax.swing.JLabel jLabel57;
    private javax.swing.JLabel jLabel58;
    private javax.swing.JLabel jLabel59;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel60;
    private javax.swing.JLabel jLabel61;
    private javax.swing.JLabel jLabel62;
    private javax.swing.JLabel jLabel63;
    private javax.swing.JLabel jLabel64;
    private javax.swing.JLabel jLabel65;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JLabel jLabel71;
    private javax.swing.JLabel jLabel8;
    private javax.swing.JLabel jLabel9;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel10;
    private javax.swing.JPanel jPanel11;
    private javax.swing.JPanel jPanel12;
    private javax.swing.JPanel jPanel13;
    private javax.swing.JPanel jPanel14;
    private javax.swing.JPanel jPanel15;
    private javax.swing.JPanel jPanel16;
    private javax.swing.JPanel jPanel17;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JPanel jPanel3;
    private javax.swing.JPanel jPanel4;
    private javax.swing.JPanel jPanel5;
    private javax.swing.JPanel jPanel6;
    private javax.swing.JPanel jPanel7;
    private javax.swing.JPanel jPanel8;
    private javax.swing.JPanel jPanel9;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JScrollPane jScrollPane2;
    private javax.swing.JTabbedPane jTabbedPane10;
    private javax.swing.JTabbedPane jTabbedPane11;
    private javax.swing.JTabbedPane jTabbedPane4;
    private javax.swing.JTabbedPane jTabbedPane8;
    private javax.swing.JLabel lblExit;
    private javax.swing.JLabel lblMin;
    private javax.swing.JPanel panelBack;
    private javax.swing.JPanel panelSideLeft;
    private javax.swing.JTable tbl_ViewStudent;
    // End of variables declaration//GEN-END:variables
}
