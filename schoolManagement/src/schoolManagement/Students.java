
package schoolManagement;

/**
 * Class for Students
 * @author Afarin
 */

public class Students {
   long id;
   String firstName;
   String lastName;
   int age;
   String gender;
   String classLecvel;
  // Date redingDate;
   String address;
   String city;
   String phone;

    public Students() {
    }

    @Override
    public String toString() {
        return "Students{" + "id=" + id + ", firstName=" + firstName + ", lastName=" + lastName + ", age=" + age + ", gender=" + gender + ", classLecvel=" + classLecvel + ", address=" + address + ", city=" + city + ", phone=" + phone + '}';
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getClassLecvel() {
        return classLecvel;
    }

    public void setClassLecvel(String classLecvel) {
        this.classLecvel = classLecvel;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

   
   
   
   
}
