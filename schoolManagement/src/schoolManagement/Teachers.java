package schoolManagement;

/**
 *
 * @author Afarin
 */
public class Teachers {

    long id;
    String firstName;
    String lastName;
    String program;

    public Teachers() {
    }

    @Override
    public String toString() {
        return "Teachers{" + "id=" + id + ", firstName=" + firstName + ", lastName=" + lastName + ", program=" + program + '}';
    }
    
    
    
}
